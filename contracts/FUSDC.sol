//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract FUSDC is ERC20 {
    constructor() ERC20("Fake USDC", "FUSDC") {
    }

    function mint(address _address, uint256 _amount) public {
        _mint(_address, _amount);
    }
}
